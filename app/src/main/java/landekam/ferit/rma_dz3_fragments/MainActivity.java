package landekam.ferit.rma_dz3_fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements FragmentEdit.TextChangeListener {
    private static final String LOG_TAG = "Bruno";
    private Button bSwitchFragments;
    public boolean mAreSwitched =false;
    private final String EDIT_FRAGMENT = "Edit_fragment";
    private final String DISPLAY_FRAGMENT = "Display_fragment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpFragments();
        this.setUpUI();
    }
    private void setUpUI() {
        this.bSwitchFragments = findViewById(R.id.bSwitchFragments);
        this.bSwitchFragments.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction =
                                fragmentManager.beginTransaction();
                        if(mAreSwitched)
                        {
                            fragmentTransaction.replace(
                                    R.id.flPrimary,new FragmentEdit(),EDIT_FRAGMENT);
                            fragmentTransaction.replace(R.id.flSecondary,new FragmentDisplay(),
                                    DISPLAY_FRAGMENT);
                        }
                        else
                        {
                            fragmentTransaction.replace(R.id.flPrimary,new FragmentDisplay(),
                                    DISPLAY_FRAGMENT);
                            fragmentTransaction.replace(R.id.flSecondary,new FragmentEdit(),EDIT_FRAGMENT);
                        }
                        fragmentTransaction.commit();
                        mAreSwitched = !mAreSwitched;
                    }
                }
        );
    }
    private void setUpFragments() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.flPrimary,new FragmentEdit(),this.EDIT_FRAGMENT);
        fragmentTransaction.add(R.id.flSecondary,new FragmentDisplay(),this.DISPLAY_FRAGMENT);
        fragmentTransaction.commit();
    }
    @Override
    public void onTextChange(int TextSize, String TextMessage, int TextColour) {
        Log.d(LOG_TAG,"In event handler");
        FragmentManager fragmentManager = getFragmentManager();
        FragmentDisplay displayFragment =
                (FragmentDisplay) fragmentManager.findFragmentByTag(this.DISPLAY_FRAGMENT);
        displayFragment.setText(TextSize,TextMessage,TextColour);
    }


}

