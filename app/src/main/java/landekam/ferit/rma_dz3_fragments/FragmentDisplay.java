package landekam.ferit.rma_dz3_fragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDisplay extends Fragment {
    TextView tvMessage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View Layout = inflater.inflate(R.layout.fragment_display,null);
        this.tvMessage = Layout.findViewById(R.id.tvMessage);
        return Layout;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void setText(int TextSize, String TextMessage, int TextColour)
    {
        this.tvMessage.setText(TextMessage);
        this.tvMessage.setTextSize(TextSize);
        if(TextColour == 0){
            this.tvMessage.setTextColor(getResources().getColor(R.color.salmon));
        }
        else if(TextColour == 1){
            this.tvMessage.setTextColor(getResources().getColor(R.color.aquamarine));
        }
        else if(TextColour == 2){
            this.tvMessage.setTextColor(getResources().getColor(R.color.turquoise));
        }
    }
}
