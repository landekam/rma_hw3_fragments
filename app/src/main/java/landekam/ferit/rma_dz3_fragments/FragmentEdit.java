package landekam.ferit.rma_dz3_fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentEdit extends Fragment {
    private int mTextSize = 10;
    private int mTextColour = 0;
    private String mTextMessage = "...";
    private EditText etTextMessage;
    private SeekBar sbTextSize;
    private Button bChangeText;
    private TextChangeListener mTextChangeListener;
    private Spinner sTextColour;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View Layout = inflater.inflate(R.layout.fragment_edit,null);
        setUpUI(Layout);
        return Layout;
    }
    private void setUpUI(View layout) {
        this.etTextMessage = layout.findViewById(R.id.etMessage);
        this.sbTextSize = layout.findViewById(R.id.sbTextSize);
        this.bChangeText = layout.findViewById(R.id.btnChangeText);
        this.sTextColour = layout.findViewById(R.id.sTextColour);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getActivity(),R.array.colours, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sTextColour.setAdapter(arrayAdapter);

        this.sbTextSize.setProgress(this.mTextSize);
        this.bChangeText.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChangeDialog();
                    }
                }
        );
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof TextChangeListener)
        {
            this.mTextChangeListener = (TextChangeListener) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.mTextChangeListener = null;
    }
    public interface TextChangeListener
    {
        void onTextChange(int TextSize, String TextMessage, int TextColour);
    }

    public void ChangeDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Change text?");
        builder.setMessage("Are you sure you want to change text?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mTextMessage = etTextMessage.getText().toString();
                mTextSize = sbTextSize.getProgress();
                mTextColour = sTextColour.getSelectedItemPosition();
                mTextChangeListener.onTextChange(mTextSize,mTextMessage,mTextColour);
            }
        });
        builder.show();
    }
}
